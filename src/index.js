import React from 'react';
import { render } from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import { Test } from './components';

render(
  <div className="App">
    <Test/>
  </div>
  , document.getElementById('root')
);
registerServiceWorker();
