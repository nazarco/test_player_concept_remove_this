import React from 'react';
import { Canvas } from 'mplayer_sdk';

export class Test extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      templateName: ''
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(action) {
    this.setState({
      templateName: action.target.value
    });
  }

  render() {
    // console.log(this.state.templateName);
    // let Tem;
    // try {
    //   Tem = require('./../templates/' + this.state.templateName).default;
    // } catch (e) {}
    const canvasEl = document.getElementById('canvas');
    const canvas = new Canvas(canvasEl);
    canvas.template.module = 'templates/Template_01.js';
    return <div>
      <input placeholder="template" onChange={this.handleChange}/>
      <h1>hello world</h1>
      <canvas id="canvas"></canvas>
    </div>;
  }
}