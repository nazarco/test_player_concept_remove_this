(function () {
    window.Template = function(app, container) {
        // Create a new texture
        var texture = this.pixi.Texture.fromImage(imageMap[0]);
        const cont = new this.pixi.Container();

        // Create a 5x5 grid of bunnies
        for (var i = 0; i < 25; i++) {
            var bunny = new this.pixi.Sprite(texture);
            bunny.anchor.set(0.5);
            bunny.x = (i % 5) * 40;
            bunny.y = Math.floor(i / 5) * 40;
            cont.addChild(bunny);
        }
        let text = new this.pixi.Text('test',{fontFamily : 'Arial', fontSize: 24, fill : 0xff1010, align : 'center'});
        container.addChild(cont);
        container.addChild(text);
        this.title = text;
        text = new this.pixi.Text('23123123t',{fontFamily : 'Arial', fontSize: 24, fill : 0xff1010, align : 'center', y: 20});
        cont.addChild(text);
        this.addText(text);

        // Move container to the center
        cont.x = app.screen.width / 2;
        cont.y = app.screen.height / 2;

        // Center bunny sprite in local container coordinates
        cont.pivot.x = cont.width / 2;
        cont.pivot.y = cont.height / 2;

        // Listen for animate update
        app.ticker.add(function(delta) {
            // rotate the container!
            // use delta to create frame-independent transform
            cont.rotation -= 0.01 * delta;
        });
    };
    var imageMap = [
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAlCAYAAABcZvm2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAWNJREFUeNrsV8sNwjAMbUqBBWACxB2pQ8AKcGALTsAJuDEFB1gBhuDAuWICmICPQh01pXWdJqEFcaglRGRbfonjPLuMc+5QwhjLGEJfZusjxZOL9akZKye9G98vPMfvsAx4qBfKwfzBL9s6uUHpI6U/u7+BKGkNb/H6umtk7MczF0HyfKS4zo/k/4AgTV8DOizrqX8oECgC+MGa8lGJp9sJDiAB8nyqYoglvJOPbP97IqoATGxWVZeXJlMQwYHA3piF8wJIblOVNBBxe3TPMLoHIKtxrbS7AAbBrA4Y5NaPAXf8LjN6wKZ0RaZOnlAFZnuXInVR4FTE6eYp0olPhhshtXsAwY3PquoAJNkIY33U7HTs7hYBwV24ItUKqDwgKF3VzAZ6k8HF+B1BMF8xRJbeJoqMXHZAAQ1kwoluURCdzepEugGEImBrIADB7I4lyfbJLlw92FKE6b5hVd+ktv4vAQYASMWxvlAAvcsAAAAASUVORK5CYII='
    ];
})();