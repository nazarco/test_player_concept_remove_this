import * as React from 'react';
import {
  Container
  , Row
  , Col
} from 'mdbreact';

var __html = require('./../templates/some.html');
var template = { __html: __html };

export default () => {
  window.helloWorld = 12;
  return (
    <Container>
      <Row>
        <Col lg="12" md="12" className="mb-lg-0 mb-12">
          <h1>Hello world!</h1>
          <div dangerouslySetInnerHTML={template} />
        </Col>
      </Row>
    </Container>
  );
}