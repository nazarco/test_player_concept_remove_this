const electron = require('electron')
const app = electron.app
const globalShortcut = electron.globalShortcut
const BrowserWindow = electron.BrowserWindow
const path = require('path')
const url = require('url')
let screen

// handle setupevents as quickly as possible
const setupEvents = require('./electron-container/setup.events')
if (setupEvents.handleSquirrelEvent()) {
  // squirrel event handled and app will exit in 1000ms, so don't do anything else
  return
}

const renderApp = () => {
  // create the browser window
  screen = new BrowserWindow()
  // render the required website/entrypoint
  // screen.loadURL('http://localhost:8080/#/')
  screen.loadURL(url.format({
    pathname: path.join(__dirname, './build/index.html'),
    protocol: 'file:',
    slashes: true
  }))
  globalShortcut.register('Ctrl+Shift+I', () => {
    screen.webContents.openDevTools()
  })

  // dereference the screen object when the window is closed
  screen.on('closed', () => {
    screen = null
  })
}

// call the renderApp() method when Electron has finished initializing
app.on('ready', renderApp)

// when all windows are closed, quit the application on Windows/Linux
app.on('window-all-closed', () => {
  // only quit the application on OS X if the user hits cmd + q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // re-create the screen if the dock icon is clicked in OS X and no other
  // windows were open
  if (screen === null) {
    renderApp()
  }
})
